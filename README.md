# Using this repo as a project

There are a number of assumptions that are made that can cause issues.  The Azure open environment in demo.redhat.com default to eastus2.  There is a default in roles > azure_lab > defaults > main.yaml that has now been updated to match that default.  If it doesn't match however you will get an error about not being able to create or move the resource group.  If this changes, make sure to explicitly set this variable in your create-infra template.
location: eastus2

# Ansible Collection - redhatautomation.azure

## Installation and Usage

### Installing the Collection

Before using the site_config collection, you need to install it.
You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: https://gitlab.com/redhatautomation/azure.git
    type: git
```

```yaml
# response_url required for slack confirmation
vars:
  response_url: x.y.z
```

### Using the collection

```yaml
---
- name: Infrastructure Provisioning
  hosts: all
  gather_facts: no

  tasks:
    - name: Create Infra
      ansible.builtin.include_role:
        name: redhatautomation.azure.azure_lab
      vars:
        activity: create-infra

    - ansible.builtin.include_role:
        name: redhatautomation.azure.azure_lab
      vars:
        activity: create-vm

- name: Windows Configuration
  hosts: all
  gather_facts: no
    - ansible.builtin.include_role:
        name: redhatautomation.azure.configure_iis

- name: Send Slack Confirmation
  hosts: localhost
  gather_facts: no

  tasks:
    - ansible.builtin.include_role:
        name: redhatautomation.azure.slack_confirm
```
