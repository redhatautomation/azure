# Azure Lab

## What this role does

The azure_lab role is comprised of multiple tasks.

- create-infra.yaml
- create-vm.yaml
- destroy-lab.yaml
- destroy-vm.yaml
- windows.yaml
- main.yaml

**main.yaml** is responsible for pulling in the other tasks, based on the activity.

**create-infra** is responsible for creating the resource group, security group to allow ssh, public ip, ip subnet, and setting up the virtual network. **destroy-lab.yaml** does the opposite of this.

**create-vm** is responsible for creating a virtual NIC, and then calls the **windows.yaml** task to finish creating a windows vm. This could be expanded to include other OS's, but for the purpose of this demo we only call windows. **destroy-vm.yaml** does the opposite of this.

### Using the role (example)

```yaml
---
- name: Deploy Azure Infrastructure
  hosts: all
  gather_facts: no

  tasks:
    - ansible.builtin.include_role:
        name: redhatautomation.azure.azure_lab
      vars:
        activity: create-infra
```

```yaml
---
- name: Deprovision Cloud
  hosts: all
  gather_facts: no

  tasks:
    - ansible.builtin.include_role:
        name: redhatautomation.azure.azure_lab
      vars:
        activity: destroy-lab
```
