# Configure IIS

## What this role does

The configure_iis role is **main.yaml** and its template files.

**main.yaml** Enables IIS if necessary (by default yes this is necessary), reboots the machine if IIS is enabled, then creates the default (C:\inetpub\wwwroot) directory, and copies the template index.html files over. We then store the public IP of the IIS server as ansible_host so it can be used elsewhere.

### Using the role (example)

```yaml
---
- name: Configure Windows IIS
  hosts: all
  gather_facts: no

  tasks:
    - ansible.builtin.include_role:
        name: redhatautomation.azure.configure_iis
```
