# Azure Slack Confirm

## What this role does

The slack_confirm role is **main.yaml** and its template files.

**main.yaml** is two tasks, one to create the response to notify the start of a job, and a confirmation to alert that the job is complete. The templates for these are in the templates and files directory.

### Using the role (example)

```yaml
---
- name: Send Slack Confirmation
  hosts: localhost
  gather_facts: no

  tasks:
    - ansible.builtin.include_role:
        name: redhatautomation.azure.slack_confirm
```
